<?php

class MoviesController extends \BaseController {

  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'layouts.default';
  protected $actor;
  protected $movie;

  public function __construct(Movie $movies, Actor $actor) {
    $this->actor = $actor;
    $this->movie = $movies;
  }

  /**
   * Display a listing of the resource.
   * GET /movies
   *
   * @return Response
   */
  public function index() {
    $movies = $this->movie->all();
    return View::make('movies.index', compact('movies'))
      ->withTitle('Movies DB - Movies')
      ->withMessage(Session::get('message'));
  }

  /**
   * Show the form for creating a new resource.
   * GET /movies/create
   *
   * @return Response
   */
  public function create() {
    return View::make('movies.create')
      ->withTitle("MovieDB - Create new Movie");
  }

  /**
   * Store a newly created resource in storage.
   * POST /movies
   *
   * @return Response
   */
  public function store() {
    //Check if request is indeed ajax
    if (Request::ajax()) {

      //Try to add new record
      $result = $this->movie->create_new(Input::all());

      //Check if there were any errors
      if ($result['code'] == '00000') {
        //Just return that everything is OK
        return 'OK';
      }
      else {
        //If there were, return error info:
        return 'Error code: ' . $result['code'] . "<br>" .
        'Error info: ' . $result['info'];
      }
    }
  }

  /**
   * Display the specified resource.
   * GET /movies/{title}
   *
   * @param  str $title
   * @return Response
   */
  public function show($title) {
    $movie = $this->movie->single($title);

    return View::make('movies.single')
      ->withActors($movie['actor-roles'])
      ->withProducers($movie['Producers'])
      ->withDirectors($movie['Directors'])
      ->withWriters($movie['Writers'])
      ->withMovie($movie['movie'])
      ->withTitle("MovieDB - {$title}");
  }

  /**
   * Show the form for editing the specified resource.
   * GET /movies/{title}/edit
   *
   * @param  string $$title
   * @return Response
   */
  public function edit($title) {
    $actors = $this->actor->all();
    $movie = $this->movie->info($title);
    return View::make('movies.edit')
      ->withTitle('Movies DB - Edit ' . $movie['title'])
      ->withActors($actors)
      ->withMovie($movie)
      ->withMessage(Session::get('message'));
  }

  /**
   * Update the specified resource in storage.
   * PUT /movies/{id}
   *
   * @return Response
   */
  public function update() {
    $old_title = Input::get('old_title');
    $new_title = Input::get('title');
    $tagline = Input::get('tagline');
    //Try to edit record
    $result = $this->movie->edit([
      'old_title' => $old_title,
      'new_title' => $new_title,
      'tagline' => $tagline
    ]);

    //Check if there were any errors
    if ($result['code'] == '00000') {
      $message['text'] = 'Movie edited successfully!';
      $message['class'] = 'alert-success changed';
      //Redirect to the new name actor page
      return Redirect::route('movies.edit', [$new_title])
        ->withMessage($message);
    }
    else {
      //If there were, return error info:
      $message['text'] = 'There was an error!<br>';
      $message['text'] .= 'Error code: ' . $result['code'] . "<br>";
      $message['text'] .= 'Error info: ' . $result['info'];
      $message['class'] = 'alert-danger changed';
      return Redirect::route('movies.edit', [$old_title])
        ->withMessage($message);
    }
  }

  /**
   * Remove the specified resource from storage.
   * DELETE /movies/{title}
   *
   * @param  int $title
   * @return Response
   */
  public function destroy($title) {
    $result = $this->movie->delete($title);
    //Check if there were any errors
    if ($result['code'] == '00000') {
      $message = 'OK';
    }
    else {
      //If there were, return error info:
      $message = 'There was an error!<br>';
      $message .= 'Error code: ' . $result['code'] . "<br>";
      $message .= 'Error info: ' . $result['info'];
    }

    return $message;
  }

}