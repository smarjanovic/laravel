<?php

class Movie extends srdjan\neo4l\Neo4l {
  protected $fillable = [];

  /**
   * Get all movies ordered by the date of release descending
   * @return array
   */
  public function all() {
    $query = $this->client->query("MATCH (movies:Movie)
                                  OPTIONAL MATCH (movies)<-[:ACTED_IN]-(actors)
                                  RETURN movies, count(actors) as actorsNum
                                  ORDER BY movies.released DESC");
    $results = [];
    foreach ($query as $key => $row) {
      $results[$key]['movie'] = $row['movies'];
      $results[$key]['actorsNum'] = $row['actorsNum'];
    }
    return $results;
  }


  /**
   * Get actors roles in a movie
   *
   * @param $actor
   * @param $movie
   * @return array
   */
  public function getActorRolesInMovie($actor, $movie) {
    $query = $this->client->query('MATCH (actor{name:"' . $actor . '"})
                                  OPTIONAL MATCH (actor)-[roles:ACTED_IN]->(movie{title:"' . $movie . '"})
                                  RETURN roles');

    $results = [];
    foreach ($query as $key => $row) {
      $results = $row['roles'];
    }
    return $results;
  }


  /**
   * Set actors roles in a movie
   *
   * @param $actor
   * @param $movie
   * @param $roles
   * @return array
   */
  public function setActorRolesInMovie($actor, $movie, $roles) {
    $roles = explode(', ', $roles);
    $roles_string = '';
    foreach ($roles as $role) {
      $roles_string .= "'$role', ";
    }
    $roles = rtrim($roles_string, ', ');

    $query = $this->client->query('MATCH (actor:Person{name:"' . $actor . '"})
                                   MATCH (movie:Movie{title:"' . $movie . '"})
                                   MERGE (actor )-[r:ACTED_IN]->(movie)
                                   SET r.roles = [' . $roles . ']');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }


  /**
   * Get movie details by it's title
   * @param str $title
   * @return array 'actors', 'directors'
   */
  public function single($title) {
    // Get movie details, actors and directors
    $query = $this->client->query('MATCH (movie:Movie{title:"' . $title . '"})
                                   OPTIONAL MATCH (movie)<-[role:ACTED_IN]-(actors)
                                   OPTIONAL MATCH (movie)<-[d:DIRECTED]-(directors)
                                   OPTIONAL MATCH (movie)<-[p:PRODUCED]-(producers)
                                   OPTIONAL MATCH (movie)<-[w:WROTE]-(writers)
                                   WITH actors, role,
                                   collect(DISTINCT directors) as Directors,
                                   collect(DISTINCT producers) as Producers,
                                   collect(DISTINCT writers) as Writers,
                                   movie
                                   RETURN movie, collect({actor: actors, roles: role.roles}) as ActorRoles, Directors, Producers, Writers
                                   ');
    $results = [];
    foreach ($query as $key => $row) {
      $results['actor-roles'] = $row['ActorRoles'];
      $results['movie'] = $row['movie'];
      $results['Directors'] = $row['Directors'];
      $results['Producers'] = $row['Producers'];
      $results['Writers'] = $row['Writers'];
    }

    return $results;
  }

  /**
   * Get basic info for a movie
   * @param $title
   * @return mixed
   */
  public function info($title) {
    $results = $this->client->query('MATCH (movie:Movie{title:"' . $title . '"}) RETURN movie');
    foreach ($results as $row) {
      $movie['title'] = $row['movie']['title'];
      $movie['tagline'] = $row['movie']['tagline'];
      $movie['released'] = $row['movie']['released'];
    }

    return $movie;
  }

  /**
   * Edit a movie
   *
   * @param array $data
   * @return array
   */
  public function edit($data) {
    $query = $this->client->query('MATCH (movie:Movie{title:"' . $data['old_title'] . '"})
                                   SET movie.title = "' . $data['new_title'] . '",
                                   movie.tagline = "' . $data['tagline'] . '"
                                   RETURN movie');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }


  /**
   * Create new movie
   *
   * @param $data ['title'], ['tagline'], ['released']
   * @return array
   */
  public function create_new($data) {
    $title = $data['title'];
    $tagline = $data['tagline'];
    $released = $data['released'];

    $query = $this->client->query('CREATE (movie:Movie{title:"' . $title . '", released:' . (int) $released . ', tagline: "' . $tagline . '"})');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }


  /**
   * Delete a movie and all its relationships, if it has any
   *
   * @param $title
   * @return array
   */
  public function delete($title) {
    $query = $this->client->query('MATCH (movie:Movie {title:"' . $title . '"})
                                   OPTIONAL MATCH (movie)-[r]-()
                                   DELETE movie, r');
    return array('code' => $query->errorCode(), 'info' => $query->errorInfo());
  }
}