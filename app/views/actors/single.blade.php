@extends('layouts.default')

@section('content')
<h1> {{ $actor['name'] }} @if(isset($actor['born'])) ({{ $actor['born'] }})@endif  </h1>
<hr/>
<div class="panel panel-primary">
  <div class="panel-heading">Acted in</div>
  <table class="actor-movies table table-striped table-bordered table-condensed">
      <thead>
          <tr>
              <th class="title">Title:</th>
              <th class="tagline">Tagline:</th>
              <th class="roles">Roles:</th>
          </tr>
      </thead>
      <tbody>
      @foreach($movies as $movie)
          <tr>
              <td><a href="/movies/{{ $movie['title'] }}">{{ $movie['title'] }} @if(isset($movie['released'])) ({{ $movie['released'] }}) @endif </a></td>
              <td>@if(!empty($movie['tagline'])) <em>"{{ $movie['tagline'] }}"</em> @endif</td>
              <td>
                  @foreach($movie['roles'] as $role)
                    {{ $role }}<br/>
                  @endforeach
              </td>
          </tr>
      @endforeach
      </tbody>
  </table>
</div>

<div class="panel panel-info">
  <div class="panel-heading">Acted with</div>

    <table class="actor-coactors table table-striped table-bordered able-condensed">
        <thead>
            <tr>
                <th class="coactor">Name:</th>
                <th class="movies">Movies in which they coacted:</th>
            </tr>
        </thead>
        <tbody>
        @foreach($coactors as $coactor)
            <tr>
                <td><a href="/actors/{{ $coactor['name'] }}">{{ $coactor['name'] }}</a></td>
                <td>
                    @foreach($coactor['movies'] as $movie)
                        <a href="/movies/{{ $movie['title'] }}">{{ $movie['title'] }} @if(isset($movie['released'])) ({{ $movie['released'] }}) @endif </a><br/>
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
</div>
@stop