@extends('layouts.default')

@section('content')
    <br/>
    {{ link_to_route('actors.create', 'Create new', [], ['class' => 'btn btn-primary pull-right']) }}

    <h1>Actors</h1>
    <div id="alerts" class="alert alert-dismissible @if(isset($message['class'])) {{ $message['class'] }} @endif" role="alert">
            <div class="message"></div>
        </div>
    <table class="actors table table-striped table-condensed">
        <thead>
            <tr>
                <th class="name">Name:</th>
                <th class="year">Year born:</th>
                <th class="roles-count">Roles played:</th>
                <td colspan="2"></td>

            </tr>
        </thead>
        <tbody>
        @foreach($actors as $actor)

        @if(!isset($actor['actor']['name']))

        <?php kd($actor); ?>
        @endif
            <tr>
               <td class="name">
                    <a href="/actors/{{$actor['actor']['name']}}">{{ $actor['actor']['name'] }}</a>
               </td>
               <td class="year">
                    @if(isset($actor['actor']['born']))
                        {{ $actor['actor']['born'] }}
                    @else
                        N/A
                    @endif
               </td>
               <td class="roles-count">
                    {{ $actor['totalRoleCount'] }}
               </td>
               <td>
                   {{ link_to_route('actors.edit', 'Edit', [$actor['actor']['name']], ['class' => 'btn btn-xs btn-warning']) }}
               </td>
               <td>
                   <a href="#" class="btn btn-xs btn-danger delete-btn" data-toggle="modal" data-target="#confirm-delete" data-entity-type="actors" data-entity="{{ $actor['actor']['name'] }}">Delete</a>
               </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Confirm Delete
                </div>
                <div class="modal-body">
                   <p>You are about to delete an actor, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

@stop