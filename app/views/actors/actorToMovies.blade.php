<table class="moviesConnect table-striped table-bordered table-condensed">
<thead>
    <tr>
      <th colspan="2">Movies to test connect with:</th>
    </tr>
</thead>
<tbody>
    @foreach($movies as $movie)
    <tr>
        <td>{{ $movie['title'] }}({{ $movie['released'] }})</td>
        <td><a href="#" class="btn btn-primary btn-connect" data-toggle="modal" data-target="#actorMovieConnectModal" data-rel="{{ $movie['title'] }}"><span class="glyphicon glyphicon-link"></span>Connect</a></td>
    </tr>
    @endforeach
</tbody>
</table>
