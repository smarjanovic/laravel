@extends('layouts.default')

@section('content')
    <h1>Edit {{ $movie['title']; }}</h1>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Movie details</h3>
      </div>
      <div class="panel-body">
        <div id="alerts" class="alert alert-dismissible @if(isset($message['class'])) {{ $message['class'] }} @endif" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" >&times;</span></button>
              @if(isset($message['text']))
                <div class="message">
                    {{ $message['text'] }}
                </div>
              @endif
        </div>

        {{ Form::open(array('method' => 'PUT', 'route' => 'movies.update')) }}
            <div class="form-group">
                {{ Form::label('title', 'Title:') }}
                {{ Form::text('title', $movie['title'], ['class'=>'form-control', 'size'=> '100']) }}
            </div>
            <div class="form-group">
                {{ Form::label('tagline', 'Tagline:') }}
                {{ Form::text('tagline', $movie['tagline'], ['class'=>'form-control', 'size'=> '100']) }}
            </div>
            <div class="form-group">
                {{ Form::label('released', 'Year released:') }}
                {{ Form::text('released', $movie['released'], ['class'=>'form-control', 'disabled' => 'disabled', 'size'=> '4']) }}
            </div>
            {{ Form::hidden('old_title', $movie['title']) }}
            <div class="from-group clearfix">
                {{ Form::submit('Edit', ['id' => 'edit-btn', 'class'=>'pull-right btn btn-success']) }}
            </div>
        {{ Form::close() }}

          <hr/>
          <div class="moviesConnect">
              <table class="table table-striped table-bordered table-condensed">
                  <thead>Connect with an actor:</thead>
                   <tbody>
                       @foreach($actors as $actor)
                       <tr>
                            <td> {{
                            link_to_route(
                                'actors.show',
                                $actor['actor']['name'],
                                [$actor['actor']['name']])
                            }}
                            @if(isset($actor['actor']['born'])) ({{ $actor['actor']['born'] }}) @endif
                            </td>
                            <td><a class="actorToMovie btn btn-primary" href="#" data-toggle="modal" data-target="#actorMovieConnectModal" data-movie="{{ $movie['title'] }}" data-actor="{{ $actor['actor']['name'] }}">Connect</a></td>
                        </tr>
                       @endforeach
                   </tbody>
              </table>
          </div>
          <hr/>
      {{ link_to_route('movies.index', 'Back to all movies', [], ['class' => 'btn btn-default pull-right']) }}
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="actorMovieConnectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('name', 'Roles:') }}
                        {{ Form::text('roles', '', ['class'=>'form-control', 'size'=> '100']) }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save"  data-dismiss="modal">Save changes</button>
                </div>
            </div>
        </div>
    </div>

@stop

