# Movies DB #
## World's First Graph Database ##

This is an example of using Neo4j database with PHP for the final project with the main topic being Graph Databases.

It is based on the Laravel 4.2 PHP framework with many Composer dependencies, but the crucial for the project is "neo4j/pdo". It enables connecting to the Neo4j database and executing Cypher queries. 

This app is built with high usage of Twitter Bootstrap 3 front-end framework.